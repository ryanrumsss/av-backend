<?php return array(
    'root' => array(
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'type' => 'package',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => NULL,
        'name' => 'avanatest/test-two',
        'dev' => true,
    ),
    'versions' => array(
        'avanatest/test-two' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'type' => 'package',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
        'shuchkin/simplexlsx' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../shuchkin/simplexlsx',
            'aliases' => array(
                0 => '9999999-dev',
            ),
            'reference' => 'c08861cfe905467a9c711c801851c9c80a164c11',
            'dev_requirement' => false,
        ),
    ),
);

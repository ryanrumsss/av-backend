<?php

namespace Avanatest\TestTwo;

use Shuchkin\SimpleXLSX;

class Validation
{
    protected $data;

    public function __construct($xlsx_path)
    {
        $this->data = SimpleXLSX::parse($xlsx_path)->rows();
    }

    function getRules()
    {
        $headers = $this->data[0];

        foreach ($headers as $columnIndex => $header) {
            $rule = null;
            (substr($header, -1) === '*') ? ($rule = 'required') : null;
            (substr($header, 0, 1) === '#') ? ($rule = 'nospace') : null;
            $rules[$columnIndex] = $rule;
        }

        return $rules;
    }

    public function validate()
    {
        $rules = $this->getRules();
        $data = $this->data;

        for ($rowIndex = 1; $rowIndex <= count($data) - 1; $rowIndex++) {
            $row = $data[$rowIndex];
            foreach ($row as $columnIndex => $item) {
                $fieldName = str_replace(['*', '#'], '', $data[0][$columnIndex]);
                if ($rules[$columnIndex] === 'required' && ($item === null || $item === '')) {
                    $errors[$rowIndex + 1][] = 'Missing value in ' . $fieldName;
                } elseif ($rules[$columnIndex] === 'nospace' && (strpos($item, ' '))) {
                    $errors[$rowIndex + 1][] = $fieldName . ' should not contain any space';
                }
            }
        }

        return $errors;
    }
}
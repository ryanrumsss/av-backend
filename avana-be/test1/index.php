<?php

function findOp($string, $index) {
    $chars = str_split($string);
    $ops = [];

    foreach ($chars as $key => $char) {
        if ($key >= $index) {
            if ($char == '(') {
                $ops[] = $key;
            } elseif ($char == ')' && count($ops) > 1) {
                array_pop($ops);
            } elseif ($char == ')' && count($ops) == 1) {
                return $key;
            }
        }
    }
}

$x = "a (b c (d e (f) g) h) i (j k)";

var_dump(findOp($x, 2));